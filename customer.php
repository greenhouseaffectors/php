<?php

require_once(__DIR__ . '/vendor/autoload.php');

use QuickBooksOnline\API\DataService\DataService;

session_start();

/*create an item class here
intuit CRUD examples https://github.com/IntuitDeveloper/SampleApp-CRUD-PHP/tree/master/CRUD_Examples
classes: https://www.php.net/manual/en/language.oop5.php
https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/customer
*/
class Customer {
    public $ID = 'aMemberVar Member Variable';
    public $name = "the customer's name";
   

    /**
     * This constructor calls the other constructors
     */
    function __construct()
    {
        $a = func_get_args();
        $i = func_num_args();
        if (method_exists($this,$f='__construct'.$i)) {
            call_user_func_array(array($this,$f),$a);
        }
    } 

    /*
    Constructors:
    https://www.php.net/manual/en/language.oop5.decon.php
    pull this item from quickbooks api by ID
    You only need enough information to display in the gui, not everything about the customer.
    */
    function __construct2( $ID, $name){
        
    }


    /**
     * Make sure everything is pushed to quickbooks.
     */
    function __destruct (){

    }


    /**
     * Returns the link where customers can be edited
     */
    public static function getCustomerLink(){
        //add a link to this page https://c50.sandbox.qbo.intuit.com/app/customers
        //but customize the url for the user's company
    }

    public static function getCustomerByID(){

    }

}
?>