<?php
    session_start();
    $_SESSION = array();
    session_destroy();
    header("Location: ./session.php");
    exit();
?>