<?php

require_once(__DIR__ . '/vendor/autoload.php');
use QuickBooksOnline\API\DataService\DataService;

$config = include('config.php');

session_start();

$dataService = DataService::Configure(array(
    'auth_mode' => 'oauth2',
    'ClientID' => $config['client_id'],
    'ClientSecret' =>  $config['client_secret'],
    'RedirectURI' => $config['oauth_redirect_uri'],
    'scope' => $config['oauth_scope'],
    'baseUrl' => "development"
));

$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
$authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

// Testing
// Store the url in PHP Session Object;
$_SESSION['authUrl'] = $authUrl;

include("./handlingSession.php");

if (!$isSessionActive) {
    header("Location: ./session.php");
    exit();
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php
        include("./components/bootstrap.php");
    ?>    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cedar Run Growers - Help</title>
    <link rel="stylesheet" href="views/common.css">
</head>
<body>
<?php
    include("./components/header.php");
?>
<div class="container help">
    <h1>Help</h1>
    <h3>Helpful Material</h3>
    <p><button  type="button" class="btn btn-success" data-toggle="modal" data-target="#onlineProjects">How to set up and use quickbooks online projects</button></p>
    <div class="modal fade" id="onlineProjects" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">How to set up and use quickbooks online projects</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/i_MJYt-i5jU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <p><button  type="button" class="btn btn-success" data-toggle="modal" data-target="#inventoryReports">How to Use Inventory Reports: Tracking, Maintaining, & More</button></p>
    <div class="modal fade" id="inventoryReports" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">How to Use Inventory Reports: Tracking, Maintaining, & More</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/J9Ta_Vxnryc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <p><button  type="button" class="btn btn-success" data-toggle="modal" data-target="#trackSold">How to Track What You've Sold: Inventory, Summary Reports, & More</button></p>
    <div class="modal fade" id="trackSold" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">How to Track What You've Sold: Inventory, Summary Reports, & More</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/WDsQXZ2ajWs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <p>Do you need help with anything else?</p>
    <p><button  type="button" class="btn btn-success" data-toggle="modal" data-target="#contactForm">Contact Us</button></p>
    <div class="modal fade" id="contactForm" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Contact Us</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <form action="/send-concern.php" method="POST">
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" class="form-control" id="subject" name="subject" aria-describedby="subjectHelp" placeholder="Enter subject">
                        <small id="subjectHelp" class="form-text text-muted">Give us a preview of your message.</small>
                    </div>
                    <div class="form-group">
                        <label for="concern">Concern</label>
                        <textarea class="form-control" id="concern" name="concern" aria-describedby="concernHelp" rows="4" placeholder="Concern..."></textarea>
                        <small id="concernHelp" class="form-text text-muted">Describe the issue you're facing.</small>
                    </div>
                    <input type="submit" class="btn btn-success">
                </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>