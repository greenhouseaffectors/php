<?php

require_once(__DIR__ . '/vendor/autoload.php');

use QuickBooksOnline\API\DataService\DataService;

session_start();

include_once("./invoice.php");
include_once("./bill.php");
include_once("./accounting-api.php");


/**
 * Quickbooks workflow: 
 * 1. Create a project when a customer wants to order flowers
 * 2. Create bill when purchasing materials to fulfill the order
 * 3. Create an invoice to sell the flowers to the customer.
 *   - The TxnDate is is when the flowers start growing
 *   - The ship date is when the order is ready
 * 
 * Enabling projects:
 * - Enable projects
 *   - >= quickbooks online plus
 * - Enable bill to customer
 * 
 * 
 * Resources:
 * - projects in the sandbox: https://c50.sandbox.qbo.intuit.com/app/projects
 * - project video: https://www.youtube.com/watch?v=i_MJYt-i5jU
 * - php docs: https://static.developer.intuit.com/sdkdocs/qbv3doc/ippphpdevkitv3/entities/index.html
 *  
 * 
 */

 /**
  * project iframe:
  * <iframe width="560" height="315" src="https://www.youtube.com/embed/i_MJYt-i5jU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  */


 /**
  * Input: N/A
  *
  * Processing: Runs all customers, all bills and all invoices through createProjectsArray and returns the result. 
  *
  * Output: see createProjectsArray 
  */
 function getAllProjects(){
    $invoices = getAllInvoices();
    $bills = getAllBills();
    $customers = getAllCustomers();
    return createProjectsArray ( $customers, $bills,  $invoices );
 }

/**
 * 
 * Input: Dates to get projects from. Dates should be of the form 'YYYY-MM-DD' for example '2015-01-01'
 *   - Note, You can use CURRENT_DATE for the dates. (The end date for example)
 *   - Note you *must* surround date parameters with single quotes. '2015-01-01' would get passed in as "'2015-01-01'" when using the function
 *   - If you leave out the endDate, it will use "CURRENT_DATE" by default
 * 
 * Processing: Gets projects with invoices and/or bills whose DueDate is between the startDate and endDate.
 *   - DueDate: The date when payment is due to a vendor or from a customer.
 * 
 * Output: see createProjectsArray
 * 
 * 
 * References:
 * - Filtering query docs: https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html#filters
 *   - "Note that not all properties support filters. Refer back to the API reference page (https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/invoice#the-invoice-object), only fields that contain keywords “filterable” are filterable."
 * - The invoice object: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/invoice#the-invoice-object
 * - Available SQL syntax: https://developer.intuit.com/app/developer/qbo/docs/develop/explore-the-quickbooks-online-api/data-queries
 *   - Includes this useful example: "SELECT * FROM Invoice WHERE TxnDate > '2011-01-01' AND TxnDate <= CURRENT_DATE"
 *
 */
 function getProjectsByDate( $startDate, $endDate = "CURRENT_DATE" ){
     $invoices = getInvoicesByDate( $startDate, $endDate );
     $bills = getBillsByDate( $startDate, $endDate );
     $customers = getAllCustomers();
     return createProjectsArray ( $customers, $bills, $invoices );
 }

 /**
  * 
  * Input: Arrays containing:
  * - Customers
  * - invoices
  * - bills
  *
  * Processing: Combines customers, invoices, and bills into arrays based on the project their in.
  * 
  * Output: An array containing the following:
  * {
  *  "projects" => {
  *     customerID: 
  *         { "customer" => customer, 
  *           "bills" => {bill, bill, ...},
  *           "invoices" => {invoice, invoice, ...} 
  *         }, 
  *     customerID2: 
  *         { "customer" => customer, 
  *           "bills" => {bill, bill, ...},
  *           "invoices" => {invoice, invoice, ...}
  *         }
  *     }
  *  "nonProjectBills" => {bill, bill, ...}
  * }
  *
  * References:
  * - php class library: https://static.developer.intuit.com/sdkdocs/qbv3doc/ippphpdevkitv3/apis/index.html
  */
function createProjectsArray ( $customers, $bills, $invoices ){
    $projectItems = array();
    $nonProjectBills = array();
    $nonProjectInvoices = array();
    foreach( $customers as $cust ){
        if( $cust->IsProject === "true" ){     
            $projectItems[ $cust->Id ] = array( "customer" => $cust, "bills" => array(), "invoices" => array() );
        }
    }
    foreach( $bills as $bill ){
        if( is_array ( $bill->Line ) ){
            $line = (array) $bill->Line[0];
        }else{
            $line = (array) $bill->Line;
        }
        $custRef = $line[ $line["DetailType"] ]->CustomerRef;
        //echo "custref: $custRef <br>";
        if( array_key_exists( $custRef, $projectItems ) ){
            $projectItems[ $custRef ]["bills"][] = $bill;
        }else{
            $nonProjectBills[] = $bill;
        }
    }
    foreach( $invoices as $inv ){
        if( array_key_exists( $inv->CustomerRef, $projectItems ) ){
            $projectItems[ $inv->CustomerRef ]["invoices"][] = $inv;
        }else{
            $nonProjectInvoices[] = $inv;
        }
    }
    return array( "projects" => $projectItems, "nonProjectBills" => $nonProjectBills, "nonProjectInvoices" => $nonProjectInvoices, "accounts" => accountsArr() );
}


/**
 * Gets all the customers
 * 
 * https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/customer
 * 
 */
 function getAllCustomers(){
    return  $_SESSION['dataService']->Query("SELECT * FROM Customer");
 }


/**
 * Runs an example of the functions in this class
 */
function projectExample(){
    $allArrays = getAllProjects();
    //$allArrays = getProjectsByDate( "'2019-02-20'" );
    prettyPrintArray( $allArrays );   
}

/**
 * Input: An array
 * 
 * Processing: Prints the array with proper spacing.
 * 
 * Output: N/A
 */
function prettyPrintArray( $arr ){
    echo("<pre>");
    print_r($arr);
    echo("</pre>");
}


/**
 * Input: N/A
 * 
 * Processing: Create the deep link to all the projects
 * 
 * Output: the deeplink
 * TODO: this requires you to be signed in with qbo for it to work. Passing a token might fix this
 */
function projectsLink(){
    return "https://qbo.intuit.com/app/projects?deeplinkcompanyid=$_SESSION[realmId]";
}

?>