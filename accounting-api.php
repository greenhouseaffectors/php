<?php

/**
 * Gets all the accounts. This is useful for the bubble chart.
 * 
 * Resources:
 * - Bills: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/bill#query-a-bill
 * - intuit CRUD examples https://github.com/IntuitDeveloper/SampleApp-CRUD-PHP/tree/master/CRUD_Examples
 * - classes: https://www.php.net/manual/en/language.oop5.php
 * - comprehensive tutorial: https://github.com/IntuitDeveloper/QBOConceptsTutorial-PHP/blob/master/LandingTheJob.php
 * - Authentication process (handled in callback.php): https://developer.intuit.com/app/developer/qbo/docs/develop/sdks-and-samples-collections/php/query-filters#individual-queries
 */

require_once(__DIR__ . '/vendor/autoload.php');

use QuickBooksOnline\API\DataService\DataService;

session_start();





/**
 * Input: N/A
 * 
 * Processing: Queries the Invoice table for all bills
 * 
 * Output: An array of all bills sorted by date descending (newest first)
 * 
 * References:
 * - Docs for queries: https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html#query-resources
 */
function getAllAccounts(){
    return $_SESSION['dataService']->Query("SELECT * FROM Account");
}

function accountsArr(){
    $acctsArr = Array();
    foreach ( getAllAccounts() as $acct ){
        $acctsArr[$acct->Id] = $acct->Name;
    }
    return $acctsArr;
}

?>