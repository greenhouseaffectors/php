<?php
/*
    This gets the redirect URI to send to the quickbooks API.
    The redirect tells the 
*/
function get_redirect_uri(){
    switch( $_ENV["GITLAB_ENVIRONMENT_URL"] ){
        case null:
            # for the development environment (remember it's inside a docker-container)
            return "http://localhost:81/callback.php";
            break;
        case "http://greenhouseaffectors-php.greenhouseaffectors.com":
            # for the production environment
            return "http://greenhouseaffectors-php.greenhouseaffectors.com/callback.php";
            break;
        default:
            # for the staging environments
            return "http://17653264-review-developmen-id0r3d.greenhouseaffectors.com/callback.php";
    }
}
# https://developer.intuit.com/app/developer/qbo/docs/develop/authentication-and-authorization/oauth-2.0#download-the-oauth-library
return array(
    'authorizationRequestUrl' => 'https://appcenter.intuit.com/connect/oauth2',
    'tokenEndPointUrl' => 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
    'client_id' => $_ENV["CLIENTID"],
    'client_secret' => $_ENV["CLIENTSECRET"],
    'oauth_scope' => 'com.intuit.quickbooks.accounting', #  openid profile email phone address
    'oauth_redirect_uri' => get_redirect_uri(),
)
?>