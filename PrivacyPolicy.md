# Privacy Policy

- All data used through this website is stored in QuickBooks Online. By accepting this document, you acknowledge you've read and agree to [Quickbook privacy statements](https://www.intuit.com/privacy/). Specifically, the privacy policy for QuickBooks Online Plus.
- No data is stored on this website, however this website will access your data from QuickBooks Online. By using our website you agree that the website may access your data stored in QuickBooks online.
- We do our best to secure our access to QuickBooks online and are following the [security requirements listed in QuickBooks](https://developer.intuit.com/app/developer/qbo/docs/list-on-the-app-store/security-requirements#app-server-configuration) but accept no liability in the case of privacy breaches, data loss, or any other cyber attacks.
- We reserve the right to collect anonymous usage statistics to improve the site.
- This document is still in its early phases. If these terms change, users will be notified on our website 30 days before the changes take place.