<?php
    $isSessionActive = false;

    $currentTime = "";

    //set the access token using the auth object
    if (isset($_SESSION['sessionAccessToken'])) {
        $isSessionActive = true;
        $accessToken = $_SESSION['sessionAccessToken'];
        $accessTokenJson = array('token_type' => 'bearer',
            'access_token' => $accessToken->getAccessToken(),
            'refresh_token' => $accessToken->getRefreshToken(),
            'x_refresh_token_expires_in' => $accessToken->getRefreshTokenExpiresAt(),
            'expires_in' => $accessToken->getAccessTokenExpiresAt()
        );
        $dataService->updateOAuth2Token($accessToken);
        $oauthLoginHelper = $dataService -> getOAuth2LoginHelper();
        $CompanyInfo = $dataService->getCompanyInfo();
        if (isset($accessTokenJson['expires_in'])) {
            $currentTime = date("h:ia",mktime(substr($accessTokenJson['expires_in'], 11, 2),substr($accessTokenJson['expires_in'], 14, 2),substr($accessTokenJson['expires_in'], 17, 2)));
        }
    }

    if ($isSessionActive) {

        $itExpiresAt = substr($accessTokenJson['expires_in'], 11, 2).substr($accessTokenJson['expires_in'], 14, 2).substr($accessTokenJson['expires_in'], 17, 2);
        $andNowIts = date("His");
        if ($andNowIts > $itExpiresAt) {
            include("./logout.php");
        }
    }
?>