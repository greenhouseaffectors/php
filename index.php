<?php

require_once(__DIR__ . '/vendor/autoload.php');
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\ReportService\ReportService;
use QuickBooksOnline\API\ReportService\ReportName;

$config = include('config.php');

session_start();

$dataService = DataService::Configure(array(
    'auth_mode' => 'oauth2',
    'ClientID' => $config['client_id'],
    'ClientSecret' =>  $config['client_secret'],
    'RedirectURI' => $config['oauth_redirect_uri'],
    'scope' => $config['oauth_scope'],
    'baseUrl' => "development"
));
$serviceContext = $dataService->getServiceContext();

$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
$authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

// Testing
// Store the url in PHP Session Object;
$_SESSION['authUrl'] = $authUrl;

include("./handlingSession.php");

if (!$isSessionActive) {
    header("Location: ./session.php");
    exit();
}

$reportService = new ReportService($serviceContext);
if (!$reportService) {
    exit("Problem while initializing ReportService.\n");
}
$reportService->setStartDate(date("Y-m-d"));
$reportService->setAccountingMethod("Accrual");
$profitAndLossReport = $reportService->executeReport(ReportName::INVENTORYVALUATIONSUMMARY);

?>
<!DOCTYPE html>
<html>
<head>
    <?php
        include("./components/bootstrap.php");
    ?>    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cedar Run Growers - Home</title>
    <link rel="stylesheet" href="views/common.css">
</head>
<body>
<?php
    include("./components/header.php");
?>
<div class="container">
    <p><a href="/summary.php" class="btn btn-primary btn-lg btn-block">Go to Summary</a></p>
    <div class="jumbotron">
        <h1 class="display-4">Hello!</h1>
        <p class="lead">Today is <?php echo date("l, F d, Y"); ?>.</p>
        <hr class="my-4">
        <h3>
            Today
            <small class="text-muted">in Numbers</small>
        </h3>
        <p>
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Inventory Valuation <small class="text-muted">Total Asset Value</small></h5>
                            <p class="display-4">$<?php if (!$profitAndLossReport) {exit();} else {print_r(end($profitAndLossReport->Rows->Row)->ColData[3]->value);}?></p>
                            <a href="https://qbo.intuit.com/app/reportv2?token=INVENTORY_VALUATION_SUM" class="btn btn-primary" target="_blank">View Inventory Valuation Summary</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Customers</h5>
                            <p class="display-4"><?php echo $_SESSION['dataService']->Query("SELECT count(*) from CUSTOMER"); ?></p>
                            <a href="https://qbo.intuit.com/app/customers" class="btn btn-primary" target="_blank">View All Customers</a>
                        </div>
                    </div>
                </div>
            </div>
        </p>
        <hr class="my-4">
        <p>
            <h5>Useful Links</h5>
            <p>
                <a href="https://qbo.intuit.com/app/homepage" class="btn btn-primary" target="_blank">Acess QuickBooks Online</a>
            </p>
            <p>
                <a href="https://qbo.intuit.com/app/items" class="btn btn-primary" target="_blank">View Inventory</a>
            </p>
        </p>
    </div>
</div>
</body>
</html>