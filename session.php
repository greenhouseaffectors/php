<?php
date_default_timezone_set('America/New_York');
require_once(__DIR__ . '/vendor/autoload.php');
use QuickBooksOnline\API\DataService\DataService;

$config = include('config.php');

session_start();

$dataService = DataService::Configure(array(
    'auth_mode' => 'oauth2',
    'ClientID' => $config['client_id'],
    'ClientSecret' =>  $config['client_secret'],
    'RedirectURI' => $config['oauth_redirect_uri'],
    'scope' => $config['oauth_scope'],
    'baseUrl' => "development"
));

$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
$authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

// Testing
// Store the url in PHP Session Object;
$_SESSION['authUrl'] = $authUrl;

include("./handlingSession.php");

?>

<!DOCTYPE html>
<html>
<head>
    <?php
        include("./components/bootstrap.php");
    ?>    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cedar Run Growers - <?php if($isSessionActive) echo 'Manage Session'; else echo 'Log In'; ?></title>
    <link rel="stylesheet" href="views/common.css">
    <script>

        var url = '<?php echo $authUrl; ?>';

        var OAuthCode = function(url) {

            this.loginPopup = function (parameter) {
                this.loginPopupUri(parameter);
            }

            this.loginPopupUri = function (parameter) {

                // Launch Popup
                var parameters = "location=1,width=800,height=650";
                parameters += ",left=" + (screen.width - 800) / 2 + ",top=" + (screen.height - 650) / 2;

                var win = window.open(url, 'connectPopup', parameters);
                var pollOAuth = window.setInterval(function () {
                    try {

                        if (win.document.URL.indexOf("code") != -1) {// This line is the problem 
                            window.clearInterval(pollOAuth);
                            win.close();
                            location.href = "/summary.php";
                        }
                    } catch (e) {
                        //DOMException: "Permission denied to access property "document" on cross-origin object"
                        console.log(e)
                    }
                }, 100);
            }
        }


        var apiCall = function() {

            this.refreshToken = function() {
                $.ajax({
                    type: "POST",
                    url: "refreshToken.php",
                }).done(function( msg ) {
                    location.reload(true);
                });
            }
        }

        var oauth = new OAuthCode(url);
    </script>
</head>
<body>
<?php
    include("./components/header.php");
?>

<div class="container login">
    <?php
        if ($isSessionActive) {
            echo '<h1>Manage Session</h1>';
            echo '<h3>You are logged in.</h2>';
            echo '<a href="/logout.php" class="btn btn-danger">Log Out</a><a href="/" class="btn btn-primary go-home">Go Home</a>';
            echo '<div id="sessionTime">';
            echo '<em>Your session expires at '.$currentTime.'</em>';
            echo '</div>';
        } else {
            echo '<h3>You haven\'t logged in. Click on the Log In button below to start your session.</h2>';
            echo '<button  type="button" class="btn btn-success" onclick="oauth.loginPopup()">Log In</button>';
        }
    ?>

</div>
</body>
</html>