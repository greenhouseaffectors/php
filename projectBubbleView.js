/**
 * This file creates the bubble chart.
 * 
 * References:
 * - Javascript include function: https://stackoverflow.com/questions/950087/how-do-i-include-a-javascript-file-in-another-javascript-file
 * 
*/

$.getScript("https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js", function() {

/****************************DATA FOR THE CHART*********************************************/
    //categories are the customer names
    var categories = new Set ();
    categories.add( "non-project" );

    //projectData and projectDataColors contain the bubbles and their colors
        //If you want to print out the data: console.table( projectData );
    var projectData = Array();// where the bubbles are and their size
    var projectDataColors = Array();

    /**
     * Adds a bubble to the chart for obj using:
     *   - date as the x value
     *   - the project name as the y value. Remember, projects in the quickbooks API are customers.
     *   - dollar amount as the bubble radius
     *   - type for the color (lightgreen for invoice, black for bill, grey for bills with $0 balance )
     * 
     * 
     * @param {an object} obj 
     * @param {the type of the object (bill or invoice)} type
     * @param {the customer (defaults to non-project)} customer
     */
    function addObjectToProjData( obj, type, customer = "non-project" ){
        var color = "";
        switch ( type){
            case "bill":
                if( obj.Balance == 0 ){
                    //balance == 0
                    projectDataColors.push( "grey" );
                }else{
                    //balance != 0
                    projectDataColors.push( "black" );
                }
                break;
            case "invoice":
                projectDataColors.push( "lightgreen" );
        }
        projectData.push( {
            //Date adapters: https://www.chartjs.org/docs/latest/axes/cartesian/time.html?h=date
            // more on dates: https://momentjs.com/docs/#/parsing/    
            x: new Date( obj["DueDate"] ),
            y: customer,
            //log2: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/log2
            r: Math.log2( Math.max ( 10, ( obj["TotalAmt"] ) ) ),
            obj: obj,
       } );
    }

    //loop over each project
    for (const proj of  Object.values(projects["projects"]) ){
        categories.add( proj["customer"]["FullyQualifiedName"] );

        //Add all the project bills to the bubble chart
        for (const bill of  proj["bills"] ){
            addObjectToProjData( bill, "bill", proj["customer"]["FullyQualifiedName"] );
        }

        //Add all the project invoices to the chart
        for (const invoice of proj["invoices"] ){
            addObjectToProjData( invoice, "invoice", proj["customer"]["FullyQualifiedName"] );
        }
    }
    
    // adding all the non-project bills to the chart
    for (const bill of projects["nonProjectBills"]  ){
        addObjectToProjData( bill, "bill", "non-project" );
    }

    // adding non-project invoices to the chart
    for (const invoice of projects["nonProjectInvoices"]  ){
        addObjectToProjData( invoice, "invoice", "non-project" );
    }

/**************************************THE ACTUAL CHART*********************************/

    /**
     * Creating the actual chart
     * 
     * References:
     * - Main bubble chart: https://www.chartjs.org/docs/2.8.0/charts/bubble.html
     */
    var ctx = document.getElementById('myChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'bubble',

        data: {
            datasets: [{
                label: ["projects"],
                backgroundColor: projectDataColors,
                borderColor: '#fff',
                data: projectData,
            },]
        },

        // Configuration options go here
        options: {
            legend: {
                //The legend would show a value per dataset, but we only have 1 dataset
                //legand configuration: https://www.chartjs.org/docs/2.8.0/configuration/legend.html#legend-label-configuration
                display: false,
            },
            //show the modal
            //onClick docs: https://www.chartjs.org/docs/2.8.0/general/interactions/events.html?h=onclick
            //see the modal code section for lineSummary
            onClick:function(event, array){
                var obj = projectData[ array[0]["_index"] ].obj;
                switch ( projectDataColors[ array[0]["_index"] ] ) {
                    case "lightgreen"://invoices
                        $('#invoiceModalTitle').text( "Invoice" ); //
                        $('#viewDetails').attr("href", `https://qbo.intuit.com/app/invoice?txnId=${obj.Id}`);
                        lineSummary( obj.Line );
                        break;
                    case "black"://bills
                        $('#invoiceModalTitle').text( "Bill" );
                        $('#viewDetails').attr("href", `https://qbo.intuit.com/app/bill?txnId=${obj.Id}`);
                        lineSummary( obj.Line );
                        break;
                    case "grey"://bills
                        $('#invoiceModalTitle').text( "Bill" );
                        $('#viewDetails').attr("href", `https://qbo.intuit.com/app/bill?txnId=${obj.Id}`);
                        lineSummary( obj.Line );
                        break;
                    default:
                        obj = null;
                        console.log("Warning: undefined behavior in the onClick function in the chart in projectBubbleView.js");
                }
                //populate and show the modal
                if( obj ){
                    $('#Total').text( obj.TotalAmt );
                    $('#DueDate').text( obj.DueDate );
                    $('#ShipDate').text( obj.ShipDate );
                    $('#Balance').text( obj.Balance );
                    $('#CustName').text( projectData[ array[0]["_index"] ].y );   
                    $('#viewInvoice').modal('show');
                }
            },
            //tooltips: https://www.chartjs.org/docs/latest/configuration/tooltip.html?h=prototype
            tooltips: {
                callbacks: {
                    //tooltip title text
                    title: function(toolTipItem, data){
                        var title = "";
                        switch ( projectDataColors[ toolTipItem[0].index ] ) {
                            case "lightgreen": //invoices
                                title = "Invoice"
                                break;
                            case "black": //bills
                                title = "Bill"
                                break;
                            case "grey": //bills
                                title = "Bill"
                                break;
                            default:
                                title = "error";
                                console.log("Warning: undefined behavior in the tooltip title in the chart in projectBubbleView.js");
                        }
                        return title;
                    },
                    // tooltip footer text
                    footer: function(tooltipItem, data){
                        return "click for more";
                    },
                    // tooltip body text
                    label: function(tooltipItem, data) {
                        var label = "error";
                        try{
                            label = "Balance: $" + projectData[ tooltipItem.index ].obj.Balance;
                        }catch(error){
                            console.log(error);
                            console.log("Warning: undefined behavior in the tooltip in the chart in projectBubbleView.js");

                        }
                        return label;
                    }
                }
            },
            scales: {
                // time scales: https://www.chartjs.org/docs/2.8.0/axes/cartesian/time.html
                xAxes: [{
                    type: "time",
                    time:{
                        max: $("#enddate").val(),
                        min: $("#startdate").val()
                    }
                }],
                //category scale: https://www.chartjs.org/docs/2.8.0/axes/cartesian/category.html
                yAxes: [
                    {
                        type: 'category', 
                        labels: Array.from( categories )
                    }
                ]
            }
        }
    }); // end chart

    //************date stuff */

//https://www.chartjs.org/docs/2.8.0/developers/updates.html
    $("#startdate").change( function () {
        //$('#startdate').data("DateTimePicker").minDate(e.date);
        
        chart.time.min = new Date(this.value);
        alert("hi" + this.value );
        chart.update();
    });
    $("#enddate").change( function (e) {
        //$('#enddate').data("DateTimePicker").maxDate(e.date);
        chart.update();
    });



    /**
     * These two functions handle updating the:
     * - date range of the chart 
     * - the min and max in the date forms, to prevent invalid inputs.
     * 
     * References:
     * - https://www.chartjs.org/docs/2.8.0/developers/updates.html
     */
    $("#startdate").change( function () {
        var date = new Date(this.value);
        chart.options.scales.xAxes[0].time.min = date;
        $("#enddate").attr({"min" : this.value});
        chart.update();
    });
    $("#enddate").change( function () {
        var date = new Date(this.value);
        chart.options.scales.xAxes[0].time.max = date;
        $("#startdate").attr({"max" : this.value});
        chart.update();
    });




/********************************INVENTORY MODAL CODE ******************************************************/


    /**
     * Takes an object's Line section and summarizes it in the itemsTable in the viewInvoice modal.
     * 
     * Input: A line object
     * 
     * Processing: sends elements from the line object to lineToTable to be added to the itemsTable.
     * 
     * Output: N/A
     * 
     */
    function lineSummary( lines ){
        //clear the table
        $("#itemsTable tr").remove();
        $("#Discount").text("0");
        if( Array.isArray( lines ) ){
            for (const line of lines ){
                lineToTable(line);
            }
        }else{
            lineToTable(lines);
        }    
    }

    /**
     * Adds information from a single line from a line object to the itemsTable in the viewInvoice modal.
     * 
     * Every line includes:
     * - Description: a description of the item
     * - Qty: the number of items (some lines don't have this)
     * - UnitPrice: the price per item (some lines don't have this)
     * - Amount: the total $ amount of this line
     * 
     * The line structure is dictated by the itemsTable.
     * 
     * 
     * @param {A single line on an object's line report} line 
     */
    function lineToTable(line){
        switch (line.DetailType){
            //****invoices****
            case "SalesItemLineDetail":
                var detail = line.SalesItemLineDetail;
                //detail.ItemRef.name ( I don't think this is needed)
                appendArrayToItemTable( [ line.Description, detail.Qty, detail.UnitPrice, line.Amount ] );
                break;
            case "DescriptionOnly":
                //used for inline subtotals in invoices ( not sure if this comment is right)
                //nothing is needed here ( it's all above)
                appendArrayToItemTable( [ line.Description, "N/A (description)", "N/A (description)", line.Amount ] );
                break;
            case "DiscountLineDetail":
                //used for overall transaction
                var detail = line.DiscountLineDetail;
                $("#Discount").text(detail.DiscountPercent);
                break;
            case "GroupLineDetail":
                var detail = line.GroupLineDetail;
                appendArrayToItemTable( [ line.Description, detail.Quantity, (line.total / detail.Quantity), line.Amount ] );
                alert("We don't currently support bundles' components. Please tell us you need this in the help");
                //TODO: make a call back to lineSummary somehow
                //https://www.youtube.com/watch?v=brf0dQTQO8o
                //line.Amount doesn't exist for GroupLine
                //detail.Quantity - quantity of the group item
                //detail.CustomerRef.name
                //detail.Line - loop as detailLine
                    //detailLine.SalesItemLineDetail.Amount
                    //detailLine.SalesItemLineDetail.Description
                    //detailLine.SalesItemLineDetail.ItemRef.value
                    //detailLine.SalesItemLineDetail.ItemRef.name

                break;
            //we don't care about "SubTotalLine": (we have Amount)
            
            //****bills****
            case "ItemBasedExpenseLineDetail":
                var detail = line.ItemBasedExpenseLineDetail;
                appendArrayToItemTable( [ line.Description, detail.Qty, detail.UnitPrice, line.Amount ] );
                break;
            case "AccountBasedExpenseLineDetail":
                var detail = line.AccountBasedExpenseLineDetail;
                appendArrayToItemTable( [ projects["accounts"][detail.AccountRef], "N/A (account based)", "N/A (account based)", line.Amount ] );
                break;
        }
    }

    /**
     * Adds the array arr to the itemsTable in the viewInvoice modal.
     * The array should have the following columns:
     * [Description, Qty, Rate, Amount]
    */
    function appendArrayToItemTable( arr ){
        $("#itemsTable").append( "<tr><td>" + arr.join("</td><td>") + "</td></tr>" );
    }


 });
