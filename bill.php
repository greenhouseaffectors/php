<?php

/**
 * When Bryan orders plugs from the plug supplier, he create a bill to record the purchase.
 * 
 * If the bill is created to fulfill a customer's order, the bill should linked to the order through a project.
 * 
 * Resources:
 * - Bills: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/bill#query-a-bill
 * - intuit CRUD examples https://github.com/IntuitDeveloper/SampleApp-CRUD-PHP/tree/master/CRUD_Examples
 * - classes: https://www.php.net/manual/en/language.oop5.php
 * - comprehensive tutorial: https://github.com/IntuitDeveloper/QBOConceptsTutorial-PHP/blob/master/LandingTheJob.php
 * - Authentication process (handled in callback.php): https://developer.intuit.com/app/developer/qbo/docs/develop/sdks-and-samples-collections/php/query-filters#individual-queries
 */

require_once(__DIR__ . '/vendor/autoload.php');

use QuickBooksOnline\API\DataService\DataService;

session_start();


/**
 * 
 * Removes all the inventory items from the bill that aren't related to the customer and subtracts the items' cost from the bill's total
 * 
 */
function billCustomer( $bill, $customerRef ){
    throw new Exception("TODO: billCustomer in bill.php hasn't been written yet.");
}



/**
 * Input: Dates between which to get bills from. Dates should be of the form 'YYYY-MM-DD' for example '2015-01-01'
 *   - Note, You can use CURRENT_DATE for the dates. (The end date for example)
 *   - Note you *must* surround date parameters with single quotes. '2015-01-01' would get passed in as "'2015-01-01'" when using the function
 * 
 * Processing: Gets bills whose DueDate is between the startDate and endDate. Note, this was TxnDate, but it got switched.
 *   - DueDate: Date when the payment of the transaction is due. If date is not provided, the number of days specified in SalesTermRef added the transaction date will be used
 *   - TxnDate: The date entered by the user when this transaction occurred.
 *   - ShipDate is what we want, but it isn't filterable.
 * 
 * Output: An array of bill objects sorted by date descending.
 * 
 * 
 * References:
 * - Filtering query docs: https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html#filters
 *   - "Note that not all properties support filters. Refer back to the API reference page (https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/invoice#the-invoice-object), only fields that contain keywords “filterable” are filterable."
 * - The bill object: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/bill
 * - Available SQL syntax: https://developer.intuit.com/app/developer/qbo/docs/develop/explore-the-quickbooks-online-api/data-queries
 *   - Includes this useful example: "SELECT * FROM Invoice WHERE TxnDate > '2011-01-01' AND TxnDate <= CURRENT_DATE"
 * 
 */
function getBillsByDate( $startDate, $endDate){
    $query = $_SESSION['dataService']->Query("SELECT * FROM Bill WHERE DueDate > $startDate AND DueDate < $endDate ORDERBY DueDate DESC");
    return $query;
}


/**
 * Input: N/A
 * 
 * Processing: Queries the Invoice table for all bills
 * 
 * Output: An array of all bills sorted by date descending (newest first)
 * 
 * References:
 * - Docs for queries: https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html#query-resources
 */
function getAllBills(){
    return $_SESSION['dataService']->Query("SELECT * FROM Bill ORDERBY DueDate DESC");
}

/**
 * Input: N/A
 * 
 * Processing: Create the deep link to all the bills
 * 
 * Output: the deeplink
 * TODO: this requires you to be signed in with qbo for it to work. Passing a token might fix this
 */
function billsLink(){
    return "https://qbo.intuit.com/app/expenses?deeplinkcompanyid=$_SESSION[realmId]";
}

/**
 * Input: N/A
 * 
 * Processing: Creates a deep link to create an invoice in the linked company the user is currently viewing.
 * 
 * Output: The deep link
 */
function createBillLink(){
    return "https://qbo.intuit.com/app/bill?deeplinkcompanyid=$_SESSION[realmId]";
}




function billExample(){

?>
<pre>

<?php
//$bills = getAllBills( );
$bills = getBillsByDate ("'2020-02-20'", "CURRENT_DATE");
echo (createBillLink());
echo ("<br>");
echo ( billsLink() );
print_r ($bills);
?>
</pre>;
<?php
}

//billExample();


?>