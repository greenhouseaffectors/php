<nav class="navbar navbar-expand-md navbar-dark bg-dark">
	<a class="navbar-brand" href="/">Cedar Run Growers</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#dropdown">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="dropdown">
		<ul class="navbar-nav ml-auto">
			<?php
				if ($isSessionActive) {
			?>
			<li class="nav-item">
				<a class="nav-link" href="/summary.php">Summary</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/">Dashboard</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="/help.php">Help</a>
			</li>
			<?php
				}
			?>
			<li class="nav-item">
				<?php
				if ($isSessionActive) {
					echo '<a class="nav-link" href="/session.php">Manage Session</a>';
				} else {
					echo '<a class="nav-link" href="/session.php">Log In</a>';
				}
				?>
			</li>
		</ul>
	</div>
</nav>