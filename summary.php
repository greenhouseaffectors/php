<?php
require_once(__DIR__ . '/vendor/autoload.php');
use QuickBooksOnline\API\DataService\DataService;

$config = include('config.php');

session_start();

$dataService = DataService::Configure(array(
    'auth_mode' => 'oauth2',
    'ClientID' => $config['client_id'],
    'ClientSecret' =>  $config['client_secret'],
    'RedirectURI' => $config['oauth_redirect_uri'],
    'scope' => $config['oauth_scope'],
    'baseUrl' => "development"
));

$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
$authUrl = $OAuth2LoginHelper->getAuthorizationCodeURL();

// Testing
// Store the url in PHP Session Object;
$_SESSION['authUrl'] = $authUrl;

include("./handlingSession.php");

if (!$isSessionActive) {
    header("Location: ./session.php");
    exit();
}

include_once("./invoice.php");
include_once("./project.php");
$projects = getAllProjects( );
?>
<!DOCTYPE html>
<html>
<head>
    <?php
        include("./components/bootstrap.php");
    ?>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cedar Run Growers - Summary</title>
    <link rel="stylesheet" href="views/common.css">
    <!--
        hidden div: https://stackoverflow.com/questions/1992114/how-do-you-create-a-hidden-div-that-doesnt-create-a-line-break-or-horizontal-sp
    -->
    <script id="projects" type="text/javascript">
        const projects = <?php echo json_encode( getAllProjects( ) )?>;
    </script>

    <!--
        php array to js array: https://stackoverflow.com/questions/5618925/convert-php-array-to-javascript/5619038#5619038
        js in separate file: https://stackoverflow.com/questions/32191226/how-to-place-javascript-in-a-separate-file-from-html#32191361
        passing variables to js form html: https://stackoverflow.com/questions/2190801/passing-parameters-to-javascript-files
    -->
    <script id="bubble" type="text/javascript" src="./projectBubbleView.js"></script>
</head>
<body>
<?php
    include("./components/header.php");
?>
<div class="container">
    <h1>Summary</h1>
    <div class="modal fade" id="viewInvoice" role="dialog">
            <div class="modal-dialog">
            <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                    <h4 class="modal-title" id="invoiceModalTitle">View</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p><strong>Customer Name:</strong> <span id="CustName"></span> </p>
                        <!-- <p><strong>Customer Reference:</strong> <div id="invoiceCustRef"></div> </p> -->
                        <p><strong>Total Amount:</strong> <span id="Total"></span> </p>
                        <p><strong>Due Date:</strong> <span id="DueDate"></span> </p>
                        <p><strong>Ship Date:</strong> <span id="ShipDate"></span> </p>
                        <p><strong>Balance:</strong> <span id="Balance"></span> </p>
                        <p><strong>Discount:</strong> <span id="Discount"></span> %</p>
                        <!--TODO: add inventory here-->
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Description</th>
                                    <th scope="col">Qty</th>
                                    <th scope="col">Rate</th>
                                    <th scope="col">Amount</th>
                                </tr>
                            </thead>
                            <tbody id="itemsTable">
                                <tr>
                                    <td>Description</td>
                                    <td>Qty</td>
                                    <td>Rate</td>
                                    <td>Amount</td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- <table id="billTable">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Amount</th>
                                    <th>Customer</th>
                                </tr>
                            </thead>
                            <tbody class="table">
                                
                            </tbody>
                        </table> -->
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-info" id="viewDetails" target="_blank" >View Details</a> <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    <!-- create bill: createBillLink() -->

    <p>
        <canvas id="myChart"></canvas>
    </p>

    <p>
        <a href="<?php echo createBillLink(); ?>" target="_blank" class="btn btn-info btn-lg">New Bill</a>
        <a href="<?php echo projectsLink(); ?>" target="_blank" class="btn btn-info btn-lg">View All Projects</a>
        <a href="<?php echo createInvoiceLink(); ?>" target="_blank" class="btn btn-info btn-lg">New Invoice</a>
        <a href="<?php echo invoicesLink(); ?>" target="_blank" class="btn btn-info btn-lg">View All Invoices</a>
    </p>
    <p>
        Date Range: 

    <?php
    $last_week = date('Y-m-d', time() - (7 * 24 * 60 * 60)); // 7 days; 24 hours; 60 mins; 60 secs) (last week)
    $next_month = date('Y-m-d', time() + (4 * 7 * 24 * 60 * 60));
    ?>
    <form action="/action_page.php">
        <div class="form-group">
            <label for="startdate">start date:</label>
            <input type="date" class="form-control" id="startdate" name="startdate" value="<?php echo $last_week ?>" max = "<?php echo $next_month?>"/>
        </div>
        <div class="form-group">
            <label for="enddate">End Date:</label>
            <input type="date" class="form-control" id="enddate" name="enddate" min="<?php echo $last_week ?>" value = "<?php echo $next_month?>">
        </div>
        <!-- <button type="submit" class="btn btn-primary">Submit</button> -->
    </form>

    </p>
</div>
</body>
</html>