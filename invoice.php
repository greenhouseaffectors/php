<?php

/*
Invoices correspond to space in the greenhouse. When a customer orders flowers, an invoice is created
and plants are placed in the greenhouse to grow. When the plants are done growing, the plants are removed
from the greenhouse, shipped, and the invoice is due.

Resources:
  - Invoices: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/invoice#the-invoice-object
  - intuit CRUD examples https://github.com/IntuitDeveloper/SampleApp-CRUD-PHP/tree/master/CRUD_Examples
  - classes: https://www.php.net/manual/en/language.oop5.php
  - comprehensive tutorial: https://github.com/IntuitDeveloper/QBOConceptsTutorial-PHP/blob/master/LandingTheJob.php
  - Authentication process (handled in callback.php): https://developer.intuit.com/app/developer/qbo/docs/develop/sdks-and-samples-collections/php/query-filters#individual-queries
*/

require_once(__DIR__ . '/vendor/autoload.php');

use QuickBooksOnline\API\DataService\DataService;

session_start();

/**
 * Input: An invoice object
 * 
 * Processing: Gets a summary of important stuff from an invoice object and puts it in an array. 
 * The summary contains: the full customer object information, the invoice balance, the total amount of the invoice, the invoice due date, the date the
 * invoice was shipped, the items in the invoice (inventory), and the full invoice object.
 * 
 * Output: The array described from above.
 * 
 * References:
 * - customer object: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/customer
 * - item object: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/item
 * - invoice object: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/invoice
 *   - "An invoice must have at least one Line for either a sales item or an inline subtotal."
 *   - "An invoice must have CustomerRef populated."
 * - We may want to take a look at estimates for certain things:
 *   - https://www.youtube.com/watch?v=HVHs5pQpPhA
 *   - https://c42.qbo.intuit.com/app/estimate?txnId=2
 */

function invoiceSummary( $qboInvoice ){
    $invoiceArr = array();

    $invoiceArr['customer'] = getCustomer($qboInvoice);
    $invoiceArr["balance"] = $qboInvoice->Balance;
    $invoiceArr['totalAmt'] = $qboInvoice->TotalAmt;
    $invoiceArr['dueDate'] = $qboInvoice->DueDate;
    $invoiceArr['shipDate'] = $qboInvoice->ShipDate;
    $invoiceArr['items'] = $qboInvoice->Line;
    $invoiceArr['fullInvoice'] = $qboInvoice;

    return $invoiceArr;
}


/**
 * Input: An invoice object
 * 
 * Processing: Gets the customer object for this invoice from quickbooks based on the required CustomerRef field
 * 
 * Output: A customer object
 * 
 * References:
 * - Read customer example: https://github.com/IntuitDeveloper/SampleApp-CRUD-PHP/blob/master/CRUD_Examples/Customer/CustomerRead.php
 * - customer object: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/customer
 * - Read customer docs: https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html#get-by-entity-id
 */
function getCustomer( $invoice ){
    return $_SESSION['dataService']->FindbyId('customer', $invoice->CustomerRef );
}


/**
 * Input: Dates between which to get invoices from. Dates should be of the form 'YYYY-MM-DD' for example '2015-01-01'
 *   - Note, You can use CURRENT_DATE for the dates. (The end date for example)
 *   - Note you *must* surround date parameters with single quotes. '2015-01-01' would get passed in as "'2015-01-01'" when using the function
 * 
 * Processing: Gets invoices whose DueDate is between the startDate and endDate. Note, this was TxnDate, but it got switched.
 *   - DueDate: Date when the payment of the transaction is due. If date is not provided, the number of days specified in SalesTermRef added the transaction date will be used
 *   - TxnDate: The date entered by the user when this transaction occurred.
 *   - ShipDate is what we want, but it isn't filterable.
 * 
 * Output: An array of invoice objects sorted by date (latest date first).
 * 
 * 
 * References:
 * - Filtering query docs: https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html#filters
 *   - "Note that not all properties support filters. Refer back to the API reference page (https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/invoice#the-invoice-object), only fields that contain keywords “filterable” are filterable."
 * - The invoice object: https://developer.intuit.com/app/developer/qbo/docs/api/accounting/most-commonly-used/invoice#the-invoice-object
 * - Available SQL syntax: https://developer.intuit.com/app/developer/qbo/docs/develop/explore-the-quickbooks-online-api/data-queries
 *   - Includes this useful example: "SELECT * FROM Invoice WHERE TxnDate > '2011-01-01' AND TxnDate <= CURRENT_DATE"
 * 
 */
function getInvoicesByDate( $startDate, $endDate){
    $query = $_SESSION['dataService']->Query("SELECT * FROM Invoice WHERE DueDate > $startDate AND DueDate < $endDate ORDERBY DueDate DESC");
    return $query;
    //These are all the fields I can tell I need. For some reason, selecting only these fields doesn't work
    //CustomerRef, Line, Balance, ARAccountRef, TotalAmt, DueDate, ShipDate
}


/**
 * Input: N/A
 * 
 * Processing: Queries the Invoice table for all invoices 
 * 
 * Output: An array of all invoices sorted by date (latest date first)
 * 
 * References:
 * - Docs for the query: https://intuit.github.io/QuickBooks-V3-PHP-SDK/quickstart.html#query-resources
 */
function getAllInvoices(){
    $inv_arr =  $_SESSION['dataService']->Query("SELECT * FROM Invoice ORDERBY DueDate DESC");
    return $inv_arr;
}

/**
 * Input: N/A
 * 
 * Processing: Create the deep link to all the invoices
 * 
 * Output: the deeplink
 * TODO: this requires you to be signed in with qbo for it to work. Passing a token might fix this
 */
function invoicesLink(){
    return "https://qbo.intuit.com/app/invoices?deeplinkcompanyid=$_SESSION[realmId]";
}

/**
 * Input: N/A
 * 
 * Processing: Creates a deep link to create an invoice in the linked company the user is currently viewing.
 * 
 * Output: The deep link
 */
function createInvoiceLink(){
    return "https://qbo.intuit.com/app/invoice?deeplinkcompanyid=$_SESSION[realmId]";
}




function example(){

?>
<pre>

<?php
//$invoices = getAllInvoices( );
$invoices = getInvoicesByDate ("'2015-01-01'", "CURRENT_DATE");
$invoice_arr = invoiceSummary( $invoices[0] );
#print_r( $invoice_arr );
print_r ( getCustomer($invoice_arr["fullInvoice"]) );
echo (createInvoiceLink());
echo ("<br>");
echo ( invoicesLink() );
?>
</pre>;
<?php
}

//example();

?>